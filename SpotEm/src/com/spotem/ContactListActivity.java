package com.spotem;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.assignment.R;
import com.spotem.data.Contact;
import com.spotem.data.ContactList;

public class ContactListActivity extends FragmentActivity implements
		ContactListFragment.Callbacks, OnCheckedChangeListener, Runnable{
	private ContactListFragment contactListFragment;
	private Handler handler = new Handler();
	private static boolean isSharing = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (savedInstanceState == null) {
			ContactList.Initalize(this);
			LocationUpdateManager.GetInstance().Init(this);
			handler.postDelayed(this, 0);
		}

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact_list);

		contactListFragment = (ContactListFragment) getSupportFragmentManager()
				.findFragmentById(R.id.contact_list);

		CheckBox cbLocSharing = (CheckBox) findViewById(R.id.cbLocSharing);
		cbLocSharing.setOnCheckedChangeListener(this);
		cbLocSharing.setChecked(isSharing);
	}

	@Override
	public void onItemSelected(String id) {
		Intent detailIntent = new Intent(this, ContactDetailActivity.class);
		detailIntent.putExtra(ContactDetailFragment.ARG_ITEM_ID, id);
		startActivity(detailIntent);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		LocationUpdateManager.GetInstance().setSharing(isChecked);
		isSharing = isChecked;
	}

	@Override
	public void run() {
		Cursor phones = getContentResolver().query(
				ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,
				null, null);
		
		while (phones.moveToNext()) {
			String name = phones
					.getString(phones
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
			String phoneNumber = phones
					.getString(phones
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
			Contact contactPhone = new Contact(phoneNumber, name);
			GetLocationTask task = new GetLocationTask();
			task.execute(contactPhone);
		}
		
		phones.close();
		handler.postDelayed(this, 3000);
		contactListFragment.FillContacts();
	}
}
