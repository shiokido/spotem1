package com.spotem.data;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.util.Log;

public class SpotEmWebservice {
	private static String IP = "128.39.42.67";
	private static String SET_URL = "http://" + IP + "/spotem/location/set/";
	private static String GET_URL = "http://" + IP + "/spotem/location/get/";
	
	public static Location getLocationFromPhoneNumber(String phoneNumber){
		String uri = GET_URL;
		uri += phoneNumber;
		
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet get = new HttpGet(uri);
		HttpResponse response;

		Location loc = new Location();
		try {
			response = httpClient.execute(get);
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 404){
				return null;
			}
			
			String result = EntityUtils.toString(response.getEntity());
			String[] split = result.split("\\s+");
			loc.latitude = Double.parseDouble(split[0]);
			loc.longitude = Double.parseDouble(split[1]);
		} catch (Exception e) {
			Log.e("Webservice error", e.toString());
			return null;
		}
		return loc;
	}
	
	public static void setLocation(String phoneNumber, Location loc){
		String uri = SET_URL;
		uri += phoneNumber + "/";
		uri += "?lat=" + loc.latitude;
		uri += "&long=" + loc.longitude;
		
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet get = new HttpGet(uri);
		try {
			HttpResponse resp = httpClient.execute(get);
			if (!EntityUtils.toString(resp.getEntity()).equals("success")){
				Log.e("Webservice", "Could not set location");
			}
		} catch (Exception e) {
			Log.e("Webservice", e.getMessage());
		}
	}
}
