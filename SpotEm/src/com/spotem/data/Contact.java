package com.spotem.data;

/**
 * A dummy item representing a piece of content.
 */
public class Contact {
	public String phoneNumber;
	public String name;
	public Location location;

	public Contact(String phoneNumber, String name) {
		this.phoneNumber = phoneNumber;
		this.name = name;
		location = new Location();
	}

	public void setLocation(Location location){
		this.location = location;
	}
	
	@Override
	public String toString() {
		if (location != null){
			return name;
		}
		return name;
	}
}