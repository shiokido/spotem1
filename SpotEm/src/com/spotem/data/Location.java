package com.spotem.data;

public class Location{
	public double latitude = -1;
	public double longitude = -1;
	
	public boolean isSet(){
		return latitude != -1 && longitude != -1;
	}
	@Override
	public String toString(){
		if (!isSet()){
			return "No location";
		}
		return "La{" + String.format("%1$,.2f}", latitude) + " Lo{" + String.format("%1$,.2f}", longitude);
	}
}