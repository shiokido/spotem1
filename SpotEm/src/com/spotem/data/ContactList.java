package com.spotem.data;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;

public class ContactList {
	/**
	 * A map of sample (dummy) items, by ID.
	 */
	public static Map<String, Contact> contacts = new HashMap<String, Contact>();
	
	public static void Initalize(Context context) {
		Contact gjovikDummy1 = new Contact("+490912736363", "GjovikTest1");
		gjovikDummy1.location = new Location();
		gjovikDummy1.location.latitude = 60.795578;
		gjovikDummy1.location.longitude = 10.691927;
		addItem(gjovikDummy1);
		
		Contact gjovikDummy2 = new Contact("+15981273812", "GjovikTest2");
		gjovikDummy2.location = new Location();
		gjovikDummy2.location.latitude = 60.788945;
		gjovikDummy2.location.longitude = 10.681626;
		addItem(gjovikDummy2);
	}

	public static void addItem(Contact contact) {
		contacts.put(contact.phoneNumber, contact);
	}
}
