package com.spotem;

import java.util.Collection;
import java.util.List;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.assignment.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.spotem.data.Contact;
import com.spotem.data.ContactList;

public class ContactDetailFragment extends Fragment {
	public static final String ARG_ITEM_ID = "item_id";
	private GoogleMap map;
	private Marker marker;
	private TextView contactText;
	
	private Contact contact;

	public ContactDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments().containsKey(ARG_ITEM_ID)) {
			contact = ContactList.contacts.get(getArguments().getString(
					ARG_ITEM_ID));
			if (contact != null) getActivity().setTitle(contact.name);
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_contact_detail,
				container, false);
		contactText = (TextView)rootView.findViewById(R.id.contact_detail);
		//contactText.setText("Distance: 12m");
		
		SupportMapFragment mapFragment = (SupportMapFragment)getActivity().getSupportFragmentManager().findFragmentById(R.id.map);	
		map = mapFragment.getMap();
		map.setMyLocationEnabled(true);
		UiSettings mapSettings = map.getUiSettings();
		mapSettings.setCompassEnabled(false);
		
		//Zoom to own location
		Location loc = LocationUpdateManager.GetInstance().locClient.getLastLocation();
		LatLng ownGeoCoords = new LatLng(loc.getLatitude(), loc.getLongitude());
		CameraUpdate positionUpdate = CameraUpdateFactory.newLatLngZoom(ownGeoCoords, 15);
		map.animateCamera(positionUpdate);
		
		if (contact != null) {
			contactText.setText("Phone: " + contact.phoneNumber);
			LatLng geoCoords = new LatLng(contact.location.latitude, contact.location.longitude);
			MarkerOptions options = new MarkerOptions();
			options.position(geoCoords);
			marker = map.addMarker(options);
		}else{
			Collection<Contact> list = ContactList.contacts.values();
			String text = "";
			
			for (Contact contact : list) {
				LatLng geoCoords = new LatLng(contact.location.latitude, contact.location.longitude);
				MarkerOptions options = new MarkerOptions();
				options.position(geoCoords);
				options.title(contact.name);
				marker = map.addMarker(options);
				marker.showInfoWindow();
				text += contact.name + ", ";
			}
			contactText.setText(text);
		}

		return rootView;
	}
	
	public static class ErrorDialogFragment extends DialogFragment {
	    private Dialog mDialog;

	    public ErrorDialogFragment() {
	        super();
	    }

	    public void setDialog(Dialog dialog) {
	        mDialog = dialog;
	    }

	    @Override
	    public Dialog onCreateDialog(Bundle savedInstanceState) {
	        return mDialog;
	    }
	}
}