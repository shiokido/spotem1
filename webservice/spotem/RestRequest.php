<?php

class RestRequest {
    private $request_vars;
    private $data;
    private $method;
    private $ressourceName;
    private $ressourceID;
    private $funcName;

    public function __construct() {
        $this->request_vars = array();
        $this->data = '';
        $this->method = 'get';
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function setMethod($method) {
        $this->method = $method;
    }

    public function setFuncName($funcName) {
        $this->funcName = $funcName;
    }

    public function setRequestVars($request_vars) {
        $this->request_vars = $request_vars;
    }

    public function setRessourceName($ressourceName) {
        $this->ressourceName = $ressourceName;
    }

    public function setRessourceID($ressourceID) {
        $this->ressourceID = $ressourceID;
    }

    public function getFuncName() {
        return $this->funcName;
    }

    public function getData() {
        return $this->data;
    }

    public function getMethod() {
        return $this->method;
    }

    public function getRequestVars() {
        return $this->request_vars;
    }

    public function getRessourceID() {
        return $this->ressourceID;
    }

    public function getRessourceName() {
        return $this->ressourceName;
    }

    public function isValidRessourceName() {
        if (empty($this->ressourceName))
            return false;
        return true;
    }

    public function isValidRessourceID() {
        if (empty($this->ressourceID))
            return false;
        
        if (is_numeric($this->ressourceID)) {
            if ((int)$this->ressourceID == $this->ressourceID) {
                if ($this->ressourceID > 0) {
                    return true;
                }
            }
        }

        return false;
    }
}

?>